package com.flipper.network;


import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.UnsupportedEncodingException;
import java.util.Map;


public class NetworkRequest<T> extends Request<T> {
    private final Gson gson = new Gson();
    private final Class<T> clazz;
    private final Map<String, String> headers;
    private final Map<String, String> parameter;
    private final Response.Listener<T> listener;
    private String jsonBody = null;

    /**
     * Make a GET request and return a parsed object from JSON.
     *
     * @param url     URL of the request to make
     * @param clazz   Relevant class object, for Gson's reflection
     * @param headers Map of request headers
     */
    public NetworkRequest(int methodType, String url, Class<T> clazz, Map<String, String> headers,
                          Response.Listener<T> listener, Response.ErrorListener errorListener, Map<String, String> parameter) {
        super(methodType, url, errorListener);

        this.clazz = clazz;
        this.headers = headers;
        this.listener = listener;
        this.parameter = parameter;
    }
    public NetworkRequest(int methodType, String url, String jsonBody, Class<T> clazz, Map<String, String> headers,
                          Response.Listener<T> listener, Response.ErrorListener errorListener, Map<String, String> parameter) {
        super(methodType, url, errorListener);
        this.clazz = clazz;
        this.headers = headers;
        this.listener = listener;
        this.parameter = parameter;
        this.jsonBody = jsonBody;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return headers != null ? headers : super.getParams();
    }
    @Override
    protected void deliverResponse(T response) {
        listener.onResponse(response);
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return parameter != null ? parameter : super.getParams();
    }

    public Map<String, String> getParameter() {
        return parameter;
    }
    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {

        try {
            String json;
            json = new String(
                    response.data,
                    HttpHeaderParser.parseCharset(response.headers));

            return Response.success(
                    gson.fromJson(json, clazz),
                    HttpHeaderParser.parseCacheHeaders(response));

        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException e) {

            return Response.error(new ParseError());
        } catch (Throwable e) {
            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected VolleyError parseNetworkError(VolleyError volleyError) {
        return super.parseNetworkError(volleyError);
    }
    @Override
    public byte[] getBody() throws AuthFailureError {

        if (jsonBody != null) {
            byte[] b = jsonBody.getBytes();
            return b;
        } else {
            return super.getBody();
        }
    }

    @Override
    public String getBodyContentType() {
        return jsonBody != null ? "application/json" : super.getBodyContentType();
    }
}