package com.flipper.network;

import android.content.Context;

import java.util.HashMap;
import java.util.Map;


public class Header {

    public static Map<String,String> getHeaders(Context context)
    {
        Map<String, String> params = new HashMap<String, String>();
        params.put("Content-Type", "application/json");
        params.put("Accept", "application/json");
        return params;
    }
}
