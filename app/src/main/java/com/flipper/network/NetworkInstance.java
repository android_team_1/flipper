package com.flipper.network;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.flipper.application.FlipperApplication;

public class NetworkInstance {

    private static NetworkInstance mInstance = null;
    private RequestQueue mRequestQueue;

    private NetworkInstance() {
        mRequestQueue = Volley.newRequestQueue(FlipperApplication.getAppContext());

    }

    public static synchronized NetworkInstance getInstance() {
        if (mInstance == null) {
            mInstance = new NetworkInstance();
        }
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        return this.mRequestQueue;
    }


}
