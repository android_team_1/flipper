package com.flipper.config;


public class AppConfig {

    public static boolean SHOULD_SHOW_TOAST = true;
    public static boolean SHOULD_SHOW_LOG = true;
    public static int NETWORK_TIME_OUT = 1000 * 60;

}