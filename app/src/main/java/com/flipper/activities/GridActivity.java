package com.flipper.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.flipper.R;
import com.flipper.adapters.GridFlipperAdapter;
import com.flipper.config.AppConfig;
import com.flipper.config.AppConstants;
import com.flipper.models.ImageResponse;
import com.flipper.network.Header;
import com.flipper.network.NetworkInstance;
import com.flipper.network.NetworkRequest;
import com.flipper.util.AppLog;


public class GridActivity extends AppCompatActivity {
    private Context context = this;
    private ProgressDialog dialog;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.grid_activity);
        initView();
        getSurveyFromServer();
    }

    private void initView() {
        dialog = new ProgressDialog(context);
        dialog.setMessage("fetching data...");
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        recyclerView = (RecyclerView) findViewById(R.id.card_recycler_view);
        layoutManager = new GridLayoutManager(getApplicationContext(), 2);
        recyclerView.setLayoutManager(layoutManager);
    }

    private void getSurveyFromServer() {
        try {
            dialog.show();
            String url = AppConstants.API_URL;
            final NetworkRequest request = new NetworkRequest(Request.Method.GET, url, ImageResponse.class, Header.getHeaders(context), new Response.Listener() {
                @Override
                public void onResponse(Object response) {
                    dialog.dismiss();
                    ImageResponse imageResponse = (ImageResponse) response;
                    GridFlipperAdapter adapter = new GridFlipperAdapter(getApplicationContext(), imageResponse.getHits());
                    recyclerView.setAdapter(adapter);

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(final VolleyError volleyError) {
                    dialog.dismiss();
                    Toast.makeText(context, "Network or parsing error", Toast.LENGTH_LONG).show();
                    AppLog.e("Response parsing error");
                }
            }, null);

            request.setRetryPolicy(new DefaultRetryPolicy(
                    AppConfig.NETWORK_TIME_OUT,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            NetworkInstance.getInstance().getRequestQueue().add(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
