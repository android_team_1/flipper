package com.flipper.adapters;

import android.animation.AnimatorInflater;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.flipper.R;
import com.flipper.application.FlipperApplication;
import com.flipper.models.Hit;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.Collections;
import java.util.List;


public class GridFlipperAdapter extends RecyclerView.Adapter<GridFlipperAdapter.ViewHolder> {
    private List<Hit> hits;
    private Context mContext;
    private boolean flag[];
    private ObjectAnimator imageAnimation;
    private ObjectAnimator textAnimation;

    public GridFlipperAdapter(Context c, List<Hit> hits) {
        mContext = c;
        this.hits = hits;
        Collections.shuffle(this.hits);
        flag = new boolean[hits.size()];
        imageAnimation = (ObjectAnimator) AnimatorInflater.loadAnimator(mContext, R.animator.object_anim);
        textAnimation = (ObjectAnimator) AnimatorInflater.loadAnimator(mContext, R.animator.object_anim);
        imageAnimation.setDuration(200);
        textAnimation.setDuration(200);
    }

    @Override
    public GridFlipperAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.grid_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final GridFlipperAdapter.ViewHolder holder, final int position) {

        //holder.flipper.setDisplayedChild(0);
        if (flag[position]) {
            holder.flipper.showNext();
        }
        holder.setIsRecyclable(false);
        if (hits.get(position).getUserImageURL() != null && !hits.get(position).getUserImageURL().equals("")) {
            ImageLoader.getInstance().displayImage(hits.get(position).getUserImageURL(), holder.imageView, FlipperApplication.imageDownloader);
        }
        holder.likes.setText("likes : " + hits.get(position).getLikes().toString());
        holder.name.setText("Image category : " + hits.get(position).getTags());
        holder.flipper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.flipper.showNext();
                flag[position] = !flag[position];
                imageAnimation.setTarget(holder.imageView);
                textAnimation.setTarget(holder.container);
                holder.flipper.setInAnimation(AnimationUtils.loadAnimation(mContext, R.anim.slid_in_from_left));
                holder.flipper.setOutAnimation(AnimationUtils.loadAnimation(mContext, R.anim.slide_out_to_right));
                textAnimation.start();
                imageAnimation.start();
            }
        });
    }

    @Override
    public int getItemCount() {
        return hits.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;
        private ViewFlipper flipper;
        private LinearLayout container;
        private TextView name, likes;

        public ViewHolder(View convertView) {
            super(convertView);
            imageView = (ImageView) convertView.findViewById(R.id.img);
            container = (LinearLayout) convertView.findViewById(R.id.container);
            flipper = (ViewFlipper) convertView.findViewById(R.id.fliper);
            name = (TextView) convertView.findViewById(R.id.name);
            likes = (TextView) convertView.findViewById(R.id.likes);
        }
    }

}