package com.flipper.application;

import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;

import com.flipper.R;
import com.flipper.util.AppLog;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.MemoryCache;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;

public class FlipperApplication extends Application {

    private static Context sContext = null;

    public static Context getAppContext() {
        return sContext;
    }

    public static DisplayImageOptions imageDownloader;

    @Override
    public void onCreate() {
        super.onCreate();
        sContext = this;
        initImageLoader(sContext);
    }

    private void initImageLoader(Context context) {
        try {

            imageDownloader = new DisplayImageOptions.Builder()
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .bitmapConfig(Bitmap.Config.RGB_565)
                    .displayer(new SimpleBitmapDisplayer())
                    .showImageForEmptyUri(R.drawable.ic_launcher)
                    .showImageOnLoading(R.drawable.ic_menu_camera)
                    .showImageOnFail(R.drawable.ic_launcher)
                    .build();

            ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(context);
            config.threadPriority(Thread.NORM_PRIORITY - 2);
            config.denyCacheImageMultipleSizesInMemory();
            config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
            config.diskCacheExtraOptions(480, 320, null);
            config.defaultDisplayImageOptions(imageDownloader);
            config.memoryCache(new WeakMemoryCache());
            config.tasksProcessingOrder(QueueProcessingType.LIFO);

            ImageLoader.getInstance().init(config.build());
        } catch (Exception e) {
            AppLog.e("Image Loading fail:" + e.getMessage());
        }
    }

    public static String appName() {
        return getAppContext().getString(R.string.app_name);
    }
}
